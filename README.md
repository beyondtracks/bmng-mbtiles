# bmng-mbtiles

Generates a raster [MBTiles](https://github.com/mapbox/mbtiles-spec) of [Blue Marble Next Generation](https://visibleearth.nasa.gov/view_cat.php?categoryID=1484).

## Build Process

1. Pick out which imagery set you want to process, for example, "DECEMBER, BLUE MARBLE NEXT GENERATION W/ TOPOGRAPHY" is `world.topo.200412`. Year ranges are `200401` through to `200412`, and map styles are `world`, `world.topo` and `world.topo.bathy`.

2. Download your imagery set with:

    ./src/download world.topo.200412

3. Apply .wld files to the downloaded imagery set (*derived from [klokan/b654b18367f372d58c58](https://gist.github.com/klokan/b654b18367f372d58c58)*) with:

    ./src/wld world.topo.200412

4. Build a VRT of the downloaded imagery set with:

    ./src/vrt world.topo.200412

5. Build an MBTiles with:

    ./src/mbtiles_gdal world.topo.200412 png

See also the [documentation for BMNG](https://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73909/readme.pdf).

## Citation
    R. St¨ockli, E. Vermote, N. Saleous, R. Simmon and D. Herring (2005). The Blue
    Marble Next Generation - A true color earth dataset including seasonal dynamics
    from MODIS. Published by the NASA Earth Observatory. Corresponding author:
    rstockli@climate.gsfc.nasa.gov
